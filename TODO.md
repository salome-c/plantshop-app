# TODO

- HTML5 / CSS https://www.w3schools.com/html/default.asp
    - formulaires
    - boites (div, span)
    - style
    - flexbox
- Bootstrap 4
- Javascript: https://www.w3schools.com/js/default.asp
    - Promesse
    - Variables
    - function et high order function
    - callback
    - filter(), map(), flatMap()
    - structures de controle: if, for, switch
- Typescript: https://www.typescriptlang.org/docs/
    - Types
    - Classes
    - Interfaces
    - Fonctions
- Pattern composant pour le web
- React 
    - https://beta.reactjs.org/learn
    - https://www.w3schools.com/react/react_components.asp
    - Utilisation des props et des state interne
    - Composants classe et composants fonctionnels
- IDE
- NodeJS et NPM
- Git: commit, créer une branche, pousser, faire un diff

## Backlog plant shop

- Tout sur la meme page pour simplifier

## Application du nommage React

Voir https://reactjs.org/docs/forms.html et les commentaires

## Réécriture de l'affichage du panier

- CartModal, au chargement de la modale, afficher un message "Chargement" (utiliser un state react "loading")
- Requeter le panier de l'utilisateur (GET /api/cart/:userId)
- Requeter les plantes du panier (GET /api/cart/:userId)
- Afficher le tout, avec prix et quantité

## Réécriture de l'achat dans la modale du panier

- Requete POST /checkout
- alert() de remerciements

## Fonctionnalité login utilisateur

- Au démarrage de l'appli, pas de login 
- En tant qu'utilisateur, je peux cliquer en haut à droite sur le bouton "login", une modale s'affiche, avec un champs texte "email"
- En tant qu'utilisateur, je clique sur login, si un panier existe sur le serveur, il est affiché, sinon rien ne se passe.

## Fonctionnalité logout utilisateur

- En tant qu'utilisateur, je clique sur logout, le panier du client (navigateur web) est vidé, le panier serveur est conservé.

## Articles recommandés

- En tant qu'utilisateur, quand j'ouvre le détail d'une plante, je veux voir les fertilisants recommandés pour ma plante.
