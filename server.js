const express = require('express')
const bodyParser = require('body-parser')

let plantStock = [
    {
        id: '7e30f0da-00b2-4c14-a49c-f0fdddec88fc',
        name: 'Jasmin',
        imgFileName: 'jasmin.jpg',
        size: 'xs',
        floweringTime: 5,
        fertilizerType: 'Minéral',
        price: 8,
        stockQuantity: 18,
        otherDetails: 'Le jasmin ça sent bon.',
        recommendedFertilizer: ['06d5b88f-e690-4d7f-a275-50cc431aa558']
    },
    {
        id: '81b4751f-f022-46d5-a5e1-31e9290dcd90',
        name: 'Lantana',
        imgFileName: 'lantana.jpg',
        size: 'md',
        floweringTime: 6,
        fertilizerType: 'Minéral',
        price: 5,
        stockQuantity: 20,
        otherDetails: 'Une fleur jolie.',
        recommendedFertilizer: ['06d5b88f-e690-4d7f-a275-50cc431aa558', 'a5f03045-514d-4cf8-84fd-0470a9bcb0ef']
    },
    {
        id: '0d731ac6-807b-47ee-a019-5a47aa91576f',
        name: 'Lavande',
        imgFileName: 'lavande.jpg',
        size: 'l',
        floweringTime: 3,
        fertilizerType: 'Organique',
        price: 11,
        stockQuantity: 21,
        otherDetails: 'La couleur de la lavande est chouette.'
    },
    {
        id: '5a947d48-9029-4e02-a4e8-0cd0c0ea6389',
        name: 'Lilas',
        imgFileName: 'lilas.jpg',
        size: 'xl',
        floweringTime: 4,
        fertilizerType: 'Organo-minéral',
        price: 7,
        stockQuantity: 23,
        otherDetails: 'Le lilas c\'est sympa.'
    },
    {
        id: 'ee377bce-aa70-4604-bf24-00d0836703e4',
        name: 'Pâquerette',
        imgFileName: 'paquerette.jpg',
        size: 'md',
        floweringTime: 1,
        fertilizerType: 'Minéral',
        price: 4,
        stockQuantity: 22,
        otherDetails: 'Les pâquerettes c\'est la base.'
    },
    {
        id: 'fc2bce1a-8f3a-438e-8cd9-d30fd1741128',
        name: 'Romarin',
        imgFileName: 'romarin.jpg',
        size: 'xs',
        floweringTime: 7,
        fertilizerType: 'Organique',
        price: 9,
        stockQuantity: 19,
        otherDetails: 'Excellente plante amoratique.'
    },
    {
        id: 'f4054107-bb7e-4d04-aab7-6354e8aaf76f',
        name: 'Rose',
        imgFileName: 'rose.jpg',
        size: 'l',
        floweringTime: 6,
        fertilizerType: 'Organo-minéral',
        price: 10,
        stockQuantity: 17,
        otherDetails: 'Les roses c\'est classe.'
    },
    {
        id: '9cabc24f-3041-4426-a9f3-20766e8a1e2a',
        name: 'Succulente',
        imgFileName: 'succulente.jpg',
        size: 'xs',
        floweringTime: 2,
        fertilizerType: 'Organo-minéral',
        price: 12,
        stockQuantity: 20,
        otherDetails: 'Même si tu n\'as pas la main verte cette plante est faite pour toi.'
    },
    {
        id: '2311087f-90e5-45c1-be7e-6da496b2680b',
        name: 'Thym',
        imgFileName: 'thym.jpg',
        size: 'xl',
        floweringTime: 1,
        fertilizerType: 'Organique',
        price: 1,
        stockQuantity: 21,
        otherDetails: 'Tout le monde sait que le thym c\'est excellent.'
    },
];

const fertilizersStock = [
    { id: '06d5b88f-e690-4d7f-a275-50cc431aa558', name: 'Guano de chauve souris', price: 99, potency: 8 },
    { id: '0698748f-e690-4d7f-a275-50cc589aa558', name: 'Guano de poulet', price: 16, potency: 3 },
    { id: 'a5f03045-514d-4cf8-84fd-0470a9bcb0ef', name: 'Horties', price: 1, potency: 5 },
]

const carts = {
    ['user-id']: {
        content: [{ plantId: '2311087f-90e5-45c1-be7e-6da496b2680b', quantity: 7 }]
    }
};

const port = 3020;
const app = express();
app.use(bodyParser.json());

app.get('/api/plants/stock', (req, res) => {
    console.log('GET /api/plants/stock')

    res.status(200).json(plantStock);
});

app.get('/api/fertilizers', (req, res) => {
    console.log('GET /api/fertilizers')

    res.status(200).json(fertilizersStock);
});

app.put('/api/plants/stock', (req, res) => {
    console.log('PUT /api/plants/stock')

    const newStock = req.body;
    if (!newStock) {
        res.status(400).json({ status: 'bad request' });
        return;
    }

    plantStock = newStock;
    res.status(200).json({ status: 'ok' });
});

app.post('/api/plants', (req, res) => {
    console.log('POST /api/plants')

    const ids = req.body;
    const plants = plantStock.filter(p => ids.includes(p.id));

    res.status(200).json(plants);
})

app.get('/api/carts/:userId', (req, res) => {
    console.log('GET /api/carts/:userId');

    const userId = req.params.userId;
    if (!userId) {
        res.status(400).json({ status: 'user id is mandatory' });
        return;
    }

    const cart = carts[userId]?.content || [];
    res.status(200).json(cart);
});

app.post('/api/carts/:userId', (req, res) => {
    console.log('POST /api/carts/:userId');

    const userId = req.params.userId;
    if (!userId) {
        res.status(400).json({ status: 'user id is mandatory' });
        return;
    }

    const content = req.body;
    if (!content) {
        res.status(400).json({ status: 'cart content is mandatory' });
        return;
    }

    carts[userId] = { ...carts[userId], content: content };
    res.status(200).json({ status: 'updated' });
});

app.post('/api/carts/:userId/checkout', (req, res) => {
    console.log('POST /api/carts/:userId/checkout');

    const userId = req.params.userId;
    if (!userId) {
        res.status(400).json({ status: 'user id is mandatory' });
        return;
    }

    const cart = carts[userId];
    if (!cart.content) {
        res.status(400).json({ status: 'user does not have a cart' });
        return;
    }

    try {
        // Update stock
        for (const { plantId, quantity } of cart.content) {
            const plantInStock = plantStock.find(p => p.id === plantId);
            if (!plantInStock) {
                throw new Error('Not in stock: ' + plantId);
            }

            if (plantInStock.stockQuantity - quantity < 0) {
                throw new Error('Not enough in stock: ' + plantId);
            }

            plantInStock.stockQuantity -= quantity;
        }

        // Empty basket
        carts[userId] = { ...carts[userId], content: [] };

        res.status(200).json({ status: 'checkout' });
    } catch (error) {
        console.log('Checkout: ', error);
        res.status(500).json({ error });
    }
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
});