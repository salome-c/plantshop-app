import './App.css';
import { Header } from './components/Header';
import { PlantList } from './components/PlantList';
import { Footer } from './components/Footer';
import { PlantDetail } from './components/PlantDetail';
import { PlantDto } from './core/PlantDto';
import React from 'react';
import { plantService } from './core/PlantService';
import { Jumbotron } from './components/Jumbotron';
import { CartModal } from './components/CartModal';

class App extends React.Component<{}, {}> {
  render() {
    return (
      <>
        <Header />
        <Jumbotron/>
        <PlantList />
        <Footer /> 
        <CartModal />
      </>
    );
  }
}

export default App;
