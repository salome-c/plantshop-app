import React, { ChangeEvent } from 'react';
import { CartItemDto } from "../core/CartItemDto";
import { cartService } from "../core/CartService";
import { PlantDto } from '../core/PlantDto';

interface Props {
    item: CartItemDto,
    plant: PlantDto;
    // FIXME: supprimer
    getCartAndAmount: () => void,

    // FIXME: utiliser à la place
    // quantity: number;
    // unitPrice: number;
    // totalPrice: number;

    onChange: () => void;
    onDelete: () => void;
}

// FIXME: supprimer
interface State {

}

export class CartItem extends React.Component<Props, State> {
    // FIXME: renommer: handleQuantityChange
    // FIXME: éxecuter la props onChange()
    // FIXME: le parent va faire cartService.update()
    changeQty = (event: ChangeEvent<HTMLInputElement>) => {
        if (Number(event.target.value) > 0 && Number.isInteger(Number(event.target.value))) {
            // cartService.changeQty_DEPRECATED(this.props.item.plant, Number(event.target.value))
            //     .then(() => this.props.getCartAndAmount())
            //     .catch(err => console.error(err));
        }
    }

    // FIXME: handleDelete
    deleteItem = () => {
        cartService.deleteItem_DEPRECATED(this.props.item)
            .then(() => this.props.getCartAndAmount())
            .catch(err => console.error(err));
    }

    render() {
        const {item} = this.props;

        return (
            <div className='d-flex justify-content-between align-items-center mb-3 mt-3'>
                {/* <label className='col-5' htmlFor={`qty${item.plant.id}`}>{item.plant.name} -  PU : {item.plant.price}€</label>
                <input 
                    className='col-2'
                    id={`qty${item.plant.id}`}
                    type="number"
                    defaultValue={`${item.qty}`}
                    onChange={this.changeQty}
                    min="1"
                    max={item.plant.stockQuantity} />
                <p className='mb-0'>{item.plant.price * item.qty}€</p> */}
                <button className="btn btn-sm btn-outline-primary col-3" onClick={this.deleteItem}>Supprimer</button>
            </div>
        );
    }
}