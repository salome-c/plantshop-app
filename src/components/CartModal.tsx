import React from "react";
import { CartItemDto } from "../core/CartItemDto";
import { cartService } from "../core/CartService";
import { PlantDto } from "../core/PlantDto";
import { plantService } from "../core/PlantService";
import { CartItem } from "./CartItem";

const modalEmitter = document.createDocumentFragment();

export function openCartModal() {
    modalEmitter.dispatchEvent(new CustomEvent('open'));
}

export function closeCartModal() {
    modalEmitter.dispatchEvent(new CustomEvent('close'));
}

interface State {
    open: boolean,
    plantsInCart: CartItemDto[],
    orderIsPurchased: boolean,
    cartAmount: number,
}

export class CartModal extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);
        this.state = {
            open: false,
            plantsInCart: [],
            orderIsPurchased: false,
            cartAmount: 0,
        }
    }

    componentDidMount(): void {
        modalEmitter.addEventListener('open', this.handleOpenEvent);
        modalEmitter.addEventListener('close', this.handleCloseEvent);
    }

    componentWillUnmount(): void {
        modalEmitter.removeEventListener('open', this.handleOpenEvent);
        modalEmitter.removeEventListener('close', this.handleCloseEvent);
    }

    handleOpenEvent = () => {
        this.setState({ open: true })
    }

    handleCloseEvent = () => {
        this.setState({ open: false })
    }

    handleClose = () => {
        this.setState({ open: false })
    }

    render() {
        const { open, plantsInCart, orderIsPurchased, cartAmount } = this.state;

        if (!open) {
            return <div></div>;
        }

        return (
            <div className="modal" tabIndex={-1} role="dialog" style={{ display: "block" }}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            {/* TODO: supprimer les escaliers de ternaires */}
                            {
                                !orderIsPurchased ?
                                    <h5 className="modal-title">Panier</h5>
                                    : <h5 className="modal-title">Tu as acheté...</h5>
                            }
                        </div>
                        <div className="modal-body">
                            {/* {
                                plantsInCart.length ?
                                    !orderIsPurchased ?
                                        plantsInCart.map(item => <CartItem item={item} key={item.plant.id} getCartAndAmount={this.getCartAndAmount}/>)
                                    :   plantsInCart.map(item => <p key={item.plant.id}>{item.qty} {item.plant.name} à {item.plant.price}€ l'unité pour {item.plant.price * item.qty}€</p>)
                                :   <p>Le panier est vide</p>
                            } */}
                        </div>
                        <div className="modal-footer d-flex justify-content-between align-items-center">
                            <p>Total : {cartAmount}€</p>
                            {
                                !orderIsPurchased && !!plantsInCart.length &&
                                <>
                                    <button type="button" className="btn btn-primary">Vider le panier</button>
                                    {/* FIXME: ajouter un message  */}
                                    <button type="button" className="btn btn-primary">Acheter</button>
                                </>
                            }
                            {/* {
                                !orderIsPurchased ?
                                    <button type="button" className="btn btn-secondary" onClick={closeCart}>Fermer</button>
                                    : <button type="button" className="btn btn-secondary" onClick={() => this.closeCartAfterPurchase()}>Fermer</button>
                            } */}

                            <button type="button" className="btn btn-secondary" onClick={this.handleClose}>Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}