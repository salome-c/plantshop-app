export const Footer = () => {
    return (
        <footer className="text-muted py-5">
            <div className="container d-flex flex-column align-items-center">
                <p className="float-right">
                    <a href="#">Revenir en haut</a>
                </p>
                <p>Salomé</p>
            </div>
        </footer>
    );
};