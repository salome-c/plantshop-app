import { useCallback, useState } from "react";
import { openCartModal } from "./CartModal";

export const Header = () => {
    const handleCartShowClick = useCallback(() => {
        openCartModal();
    }, []);

    return (
        <header>
            <div className="navbar navbar-dark bg-dark box-shadow">
                <div className="container d-flex justify-content-between">
                    <a href="#" className="navbar-brand d-flex align-items-center">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="20" viewBox="0 0 407.814 407.814">
                            <g>
                                <path d="M353.985,203.905c35.145-20.995,47.049-66.379,26.508-101.954c-20.54-35.575-65.794-47.957-101.547-28.017
                                C278.336,33.001,244.985,0,203.907,0c-41.079,0-74.431,33.001-75.039,73.937c-35.754-19.94-81.01-7.557-101.549,28.02
                                C6.78,137.531,18.683,182.915,53.83,203.908c-35.146,20.995-47.05,66.379-26.51,101.954c20.54,35.576,65.796,47.958,101.55,28.017
                                c0.607,40.936,33.96,73.938,75.039,73.936c41.08,0,74.43-33.002,75.038-73.936c35.754,19.939,81.011,7.557,101.548-28.021
                                C401.035,270.282,389.13,224.898,353.985,203.905z M203.907,272.65c-37.966,0-68.744-30.777-68.744-68.744
                                s30.778-68.744,68.744-68.744c37.965,0,68.744,30.777,68.744,68.744S241.873,272.65,203.907,272.65z"/>
                            </g>
                        </svg>
                        <strong>Boutique de plantes et fleurs</strong>
                    </a>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarHeader"
                        aria-controls="navbarHeader"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                        onClick={handleCartShowClick}
                    >
                        <svg viewBox="0 0 512 512" width="40" height="50" xmlns="http://www.w3.org/2000/svg">
                            <g data-name="1" id="_1">
                                <path d="M397.78,316H192.65A15,15,0,0,1,178,304.33L143.46,153.85a15,15,0,0,1,14.62-18.36H432.35A15,15,0,0,1,447,153.85L412.4,304.33A15,15,0,0,1,397.78,316ZM204.59,286H385.84l27.67-120.48H176.91Z" /><path d="M222,450a57.48,57.48,0,1,1,57.48-57.48A57.54,57.54,0,0,1,222,450Zm0-84.95a27.48,27.48,0,1,0,27.48,27.47A27.5,27.5,0,0,0,222,365.05Z" /><path d="M368.42,450a57.48,57.48,0,1,1,57.48-57.48A57.54,57.54,0,0,1,368.42,450Zm0-84.95a27.48,27.48,0,1,0,27.48,27.47A27.5,27.5,0,0,0,368.42,365.05Z" /><path d="M158.08,165.49a15,15,0,0,1-14.23-10.26L118.14,78H70.7a15,15,0,1,1,0-30H129a15,15,0,0,1,14.23,10.26l29.13,87.49a15,15,0,0,1-14.23,19.74Z" />
                            </g>
                        </svg>
                    </button>
                </div>
            </div>
        </header>
    );
}