export const Jumbotron = () => {
    return (
        <section className="jumbotron text-center mb-0 bg-white">
            <div className="container">
                <h1 className="jumbotron-heading">Bienvenue</h1>
                <p className="lead text-muted">
                    Voici la meilleure boutique de plantes et fleurs
                </p>
            </div>
        </section>
    );
};