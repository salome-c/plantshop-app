import { PlantDto } from "../core/PlantDto";

interface Props {
    plant: PlantDto,
    onClose: () => void,
}

export const PlantDetail = (props: Props) => {
    const { plant, onClose: handleClose } = props;

    return (
        // FIXME: expliquer toutes les propriétés ou nettoyer 
        <div className="modal" role="dialog" style={{ display: "block" }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{plant.name}</h5>
                    </div>
                    <div className="modal-body">
                        <p>Identifiant : {plant.id}</p>
                        <p>Prix unitaire : {plant.price}€</p>
                        <p>Type d'engrais : {plant.fertilizerType}</p>
                        <p>Temps de floraison : {plant.floweringTime} jours</p>
                        <p>Taille :
                            {
                                {
                                    'xs': ' Petit',
                                    'md': ' Moyen',
                                    'l': ' Grand',
                                    'xl': ' Très grand'
                                }[plant.size]
                            }
                        </p>
                        <p>Autres détails : {plant.otherDetails}</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={handleClose}>Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    );

}