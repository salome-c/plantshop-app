import React, { ChangeEvent } from 'react';
import { PlantDto } from '../core/PlantDto';
import { CartItemDto } from '../core/CartItemDto';

interface Props {
    plant: PlantDto,
    maxQuantity: number,
    onDetailClick: (plant: PlantDto) => void,
    onAddToCart: (item: CartItemDto) => void,
    onFastPurchase: (item: CartItemDto) => void,
}

interface State {
    quantity: number,
}

export class PlantItem extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { quantity: 1 };
    }

    render() {
        const { plant, maxQuantity } = this.props;
        const { quantity } = this.state;

        return (
            <div className="card mb-4 box-shadow">
                <img
                    className="card-img-top"
                    style={{ height: "300px" }}
                    src={require(`../img/${plant.imgFileName}`)}
                    alt={`${plant.name}`}
                />
                <div className="card-body">
                    <div className="d-flex justify-content-between align-items-center flex-wrap">
                        <p className="card-text">{plant.name}</p>
                        <p>{plant.price}€</p>
                        <button
                            type="button"
                            className="btn btn-sm btn-outline-secondary mb-3"
                            onClick={this.handleDetailClick}>
                            Voir le détail
                        </button>
                        <form className="d-flex justify-content-between align-items-center flex-wrap">
                            <label htmlFor={`qty${plant.id}`}>Quantité - max {maxQuantity} :</label>
                            <input
                                id={`qty${plant.id}`}
                                type="number"
                                min="1"
                                max={`${maxQuantity}`}
                                defaultValue="1"
                                onChange={this.handleQuantityChange} />
                            <button
                                type="button"
                                className="btn btn-sm btn-outline-primary mt-3"
                                onClick={this.handleAddToCart}>
                                Ajouter au panier
                            </button>
                            <button
                                type="button"
                                className="btn btn-sm btn-outline-primary mt-3"
                                onClick={this.handleFastPurchase}>
                                Achat rapide
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    private handleDetailClick = () => {
        this.props.onDetailClick(this.props.plant);
    }

    private handleQuantityChange = (ev: ChangeEvent<HTMLInputElement>) => {
        this.setState({ quantity: parseInt(ev.target.value) });
    }

    private handleAddToCart = () => {
        this.props.onAddToCart({plantId: this.props.plant.id, quantity: this.state.quantity});
    }

    private handleFastPurchase = () => {
        this.props.onFastPurchase({plantId: this.props.plant.id, quantity: this.state.quantity});
    }
}