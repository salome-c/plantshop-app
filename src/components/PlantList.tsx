import { PlantItem } from './PlantItem';
import { plantService } from '../core/PlantService';
import { PlantDto } from '../core/PlantDto';
import React from 'react';
import { PlantDetail } from './PlantDetail';
import { CartItemDto } from '../core/CartItemDto';
import { cartService } from '../core/CartService';

interface State {
    plants: PlantDto[],
    detailModal: false | PlantDto,
}

export class PlantList extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);
        this.state = {
            plants: [],
            detailModal: false,
        }
    }

    componentDidMount(): void {
        this.refreshList();
    }

    refreshList() {
        plantService.getStock()
            .then(plants => this.setState({ plants }))
            .catch(err => console.error('Get stock error:', err));
    }

    handleDetailClick = (plant: PlantDto) => {
        this.setState({ detailModal: plant });
    }

    handleCloseDetailClick = () => {
        this.setState({ detailModal: false });
    }

    addToCart = (item: CartItemDto) => {
        return cartService.getCart()
            .then(cart => {
                const alreadyInCart = cart.find(i => i.plantId === item.plantId);
                if (alreadyInCart) {
                    alreadyInCart.quantity += item.quantity;
                } else {
                    cart.push(item);
                }

                return cartService.updateCart(cart);
            })
    }

    handleAddToCartClick = (item: CartItemDto) => {
        this.addToCart(item)
            .then(() => this.refreshList())
            .catch(err => console.error('Add to cart error: ', err));
    }

    handleFastPurchase = (item: CartItemDto) => {
        this.addToCart(item)
            .then(() => cartService.checkout())
            .then(() => this.refreshList())
            .then(() => alert('Merci pour votre achat'))
            .catch(err => {
                console.error('Add to cart error: ', err);
                alert('Erreur, peut-être plus assez en stock ?');
            });
    }

    render() {
        const { plants, detailModal } = this.state;

        return (
            <main role="main">
                {!plants.length && <div>Pas de plante pour le moment</div>}

                {!!plants.length &&
                    <div className="album py-5 bg-light">
                        <div className="container">
                            <div className="row">
                                {plants.map((plant) =>
                                    <div key={plant.id} className="col-md-4">
                                        <PlantItem
                                            plant={plant}
                                            maxQuantity={plant.stockQuantity}
                                            onDetailClick={this.handleDetailClick}
                                            onAddToCart={this.handleAddToCartClick}
                                            onFastPurchase={this.handleFastPurchase} />
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>}

                {detailModal && <PlantDetail plant={detailModal} onClose={this.handleCloseDetailClick} />}
            </main>
        );
    }

}