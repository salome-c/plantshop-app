import { PlantDto } from "./PlantDto";

export interface CartItemDto {
    plantId: string,
    quantity: number
}