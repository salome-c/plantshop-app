import { CartItemDto } from "./CartItemDto";
import { PlantDto } from "./PlantDto";
import axios from 'axios';
import { userService } from "./UserService";

let cart: CartItemDto[] = [];

const client = axios.create({ timeout: 5000, responseType: 'json' });

export class CartService {
    // TODO: delete
    // TODO: delete
    public async addToCart_DEPRECATED(plant: PlantDto, qty: number): Promise<void>{
        // for (let item of cart) {
        //     if (item.plant === plant) {
        //         item.qty += qty;
        //         return;
        //     }
        // }
        // const plantToAdd: CartItemDto = {plant, qty};
        // cart.push(plantToAdd);
    }

    // TODO: delete
    public async changeQty_DEPRECATED(plant: PlantDto, qty: number): Promise<void>{
        // const newItem: CartItemDto = {plant, qty};
        // cart = cart.map(item => plant === item.plant ? newItem : item);
    }

    // TODO: delete
    public async cleanCart_DEPRECATED(): Promise<void> {
        cart = [];
    }

    public async deleteItem_DEPRECATED(item: CartItemDto): Promise<void> {
        cart = cart.filter(i => !(i === item));
    }


    public async getCart(): Promise<CartItemDto[]> {
        return client.get('/api/carts/' + userService.getCurrentLogin()).then(result => result.data);
    }

    public async updateCart(cart: CartItemDto[]): Promise<void> {
        return client.post('/api/carts/' + userService.getCurrentLogin(), cart).then(() => undefined);
    }

    public async checkout(): Promise<void> {
        return client.post('/api/carts/' + userService.getCurrentLogin() + '/checkout', cart).then(() => undefined);
    }
}

export const cartService = new CartService();