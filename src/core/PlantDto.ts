export interface PlantDto {
    id: string;
    name: string;
    imgFileName: string;
    size: 'xs'|'md'|'l'|'xl';
    floweringTime: number;
    fertilizerType: 'Minéral'|'Organique'|'Organo-minéral';
    price: number;
    stockQuantity: number;
    otherDetails: string;
}