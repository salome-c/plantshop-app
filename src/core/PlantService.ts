import { PlantDto } from "./PlantDto";
import axios from 'axios';

const client = axios.create({ timeout: 5000, responseType: 'json' });

export class PlantService {
    public async getStock(): Promise<PlantDto[]> {
        return client.get('/api/plants/stock').then(result => result.data);
    }

    public async updateStock(plants: PlantDto[]): Promise<void> {
        return client.put('/api/plants/stock', plants).then(() => undefined);
    }

    public getByIds(plantIds: string[]): Promise<PlantDto[]> {
        return client.post('/api/plants').then(result => result.data);
    }
}

export const plantService = new PlantService();