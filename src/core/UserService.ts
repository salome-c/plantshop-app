class UserService {
    private currentLogin = Date.now() + '@anonymous.net';

    public async login(email: string): Promise<void> {
        this.currentLogin = email;
    }

    public async logout(): Promise<void> {
        this.currentLogin = Date.now() + '@anonymous.net';
    }

    public getCurrentLogin() {
        return this.currentLogin;
    }
}

export const userService = new UserService();